package com.teamapt.filemanagementservice.config;

import com.teamapt.filemanagementservice.concrete.LocalStorageFileManager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
   @Bean
   CommandLineRunner init(LocalStorageFileManager localStorageFileManager) {
      return (args) -> {
         localStorageFileManager.init();
      };
   }
}
