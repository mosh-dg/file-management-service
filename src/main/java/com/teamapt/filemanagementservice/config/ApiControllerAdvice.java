package com.teamapt.filemanagementservice.config;

import com.teamapt.api.ApiResponseEnvelope;
import com.teamapt.api.ApiRestControllerAdvice;
import com.teamapt.exceptions.ApiError;
import java.util.Iterator;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestControllerAdvice
public class ApiControllerAdvice extends ApiRestControllerAdvice {
   @ExceptionHandler({MethodArgumentNotValidException.class})
   public ResponseEntity<Object> handleBadRequest(MethodArgumentNotValidException ex, WebRequest request) {
      ApiResponseEnvelope envelope = new ApiResponseEnvelope(false);
      BindingResult result = ex.getBindingResult();
      List<FieldError> errors = result.getFieldErrors();
      Iterator var6 = errors.iterator();

      while(var6.hasNext()) {
         FieldError err = (FieldError)var6.next();
         ApiError ae = new ApiError(err.getDefaultMessage(), 44);
         envelope.addError(ae);
      }

      return new ResponseEntity(envelope, HttpStatus.BAD_REQUEST);
   }
}
