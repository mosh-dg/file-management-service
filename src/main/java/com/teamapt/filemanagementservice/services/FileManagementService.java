package com.teamapt.filemanagementservice.services;

import com.teamapt.exceptions.ApiException;
import com.teamapt.exceptions.BadRequestApiException;
import com.teamapt.exceptions.NotFoundApiException;
import com.teamapt.filemanagementservice.concrete.FileManagerFactory;
import com.teamapt.filemanagementservice.contracts.FileManager;
import com.teamapt.filemanagementservice.model.entity.Client;
import com.teamapt.filemanagementservice.model.entity.File;
import com.teamapt.filemanagementservice.repositories.ClientRepository;
import com.teamapt.filemanagementservice.repositories.FileRepository;
import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.filemanagementservicelib.request.FileUploadRequest;
import com.teamapt.filemanagementservicelib.response.FileDTO;
import com.teamapt.filemanagementservicelib.response.FileDTOWrapper;
import com.teamapt.filemanagementservicelib.response.FileDeleteResponse;
import com.teamapt.filemanagementservicelib.response.FileUploadActionResponse;
import com.teamapt.filemanagementservicelib.response.FileUploadResponse;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.spi.FileTypeDetector;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class FileManagementService {
   private final FileRepository fileRepository;
   private final ClientRepository clientRepository;
   private final FileManagerFactory fileManagerFactory;
   private static final String CLIENT_ID = "MONEYTOR";
   private static final String EMPTY_FILE = "File cannot be null";
   private static final String INVALID_FILE_ARGUMENTS = "FileUUID and ClientId must be provided";
   private static final Logger LOGGER = LoggerFactory.getLogger(FileManagementService.class.getName());

   @Autowired
   public FileManagementService(FileRepository fileRepository, ClientRepository clientRepository, FileManagerFactory fileManagerFactory) {
      this.fileRepository = fileRepository;
      this.clientRepository = clientRepository;
      this.fileManagerFactory = fileManagerFactory;
   }

   public FileUploadResponse saveFile(FileUploadRequest request) throws ApiException {
      if (request == null) {
         throw new BadRequestApiException("File cannot be null");
      } else if (request.getFile() == null) {
         throw new BadRequestApiException("File cannot be null");
      } else {
         FileManager fileManager = this.getFileManager("MONEYTOR");
         FileUploadActionResponse response;
         if (request.getFileLocation() == null) {
            response = fileManager.saveFile(request.getFile());
         } else {
            response = fileManager.saveFile(request.getFile(), request.getFileLocation());
         }

         if (response.isSuccessful()) {
            this.fileRepository.save(this.buildFileModel(response, "MONEYTOR"));
         }

         return new FileUploadResponse(response);
      }
   }

   public FileUploadResponse saveFiles(FileUploadRequest request) throws ApiException {
      if (request == null) {
         throw new BadRequestApiException("File cannot be null");
      } else if (request.getFiles() != null && !request.getFiles().isEmpty()) {
         FileManager fileManager = this.getFileManager("MONEYTOR");
         List<File> fileList = new ArrayList();
         List responses;
         if (request.getFileLocation() == null) {
            responses = fileManager.saveFiles(request.getFiles());
         } else {
            responses = fileManager.saveFiles(request.getFiles(), request.getFileLocation());
         }

         Iterator var5 = responses.iterator();

         while(var5.hasNext()) {
            FileUploadActionResponse response = (FileUploadActionResponse)var5.next();
            if (response.isSuccessful()) {
               fileList.add(this.buildFileModel(response, "MONEYTOR"));
            }
         }

         this.fileRepository.save(fileList);
         return new FileUploadResponse(responses);
      } else {
         throw new BadRequestApiException("File cannot be null");
      }
   }

   public Resource loadFile(String fileUUID) throws ApiException {
      File file = this.validateFile(fileUUID, "MONEYTOR");
      FileManager fileManager = this.fileManagerFactory.getFileManager(file.getStorageSystem());
      return fileManager.loadFile(file);
   }

   public FileDTO loadFileAsDTO(String fileUUID) throws ApiException {
      FileTypeDetector detector = new TikaFileTypeDetector();
      File file = this.validateFile(fileUUID, "MONEYTOR");
      FileManager fileManager = this.fileManagerFactory.getFileManager(file.getStorageSystem());
      Resource resource = fileManager.loadFile(file);
      FileDTO fileDTO = new FileDTO();

      String contentType;
      try {
         contentType = detector.probeContentType(Paths.get(file.getFileLocation() + "/" + file.getFileName()));
      } catch (IOException var11) {
         LOGGER.error("Error occurred while loading files as DTO due to ", var11);
         contentType = "UNKNOWN";
      }

      fileDTO.setContentType(contentType);
      fileDTO.setFileLocation(file.getFileLocation());
      fileDTO.setFileName(file.getFileName());
      fileDTO.setOriginalFileName(file.getOriginalFileName());
      fileDTO.setFileSizeBytes(file.getFileSizeBytes());
      fileDTO.setFileUUID(file.getUuid());
      fileDTO.setUploadedTime(file.getTimeAdded().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
      fileDTO.setStorageSystem(file.getStorageSystem());
      fileDTO.setSuccessful(true);
      if (contentType.contains("image/")) {
         try {
            byte[] bytes = IOUtils.toByteArray(resource.getInputStream());
            String encoded = Base64.getEncoder().encodeToString(bytes);
            fileDTO.setBase64String(encoded);
         } catch (IOException var10) {
            LOGGER.error("Error occurred while loading files as DTO due to ", var10);
            fileDTO.setBase64String((String)null);
         }
      }

      return fileDTO;
   }

   public FileDTOWrapper loadFilesAsDTO(List<String> fileUUIDs) throws ApiException {
      List<FileDTO> fileDTOS = new ArrayList();
      FileDTOWrapper wrapper = new FileDTOWrapper();
      if (fileUUIDs != null && !fileUUIDs.isEmpty()) {
         FileDTO fileDTO;
         for(Iterator var4 = fileUUIDs.iterator(); var4.hasNext(); fileDTOS.add(fileDTO)) {
            String uuid = (String)var4.next();

            try {
               fileDTO = this.loadFileAsDTO(uuid);
            } catch (ApiException var8) {
               LOGGER.error("Error occurred while loading files as DTO due to ", var8);
               fileDTO = new FileDTO();
               fileDTO.setSuccessful(false);
            }
         }

         wrapper.setFileDTOS(fileDTOS);
         return wrapper;
      } else {
         return wrapper;
      }
   }

   public FileDeleteResponse deleteFiles(List<String> fileUUIDs) {
      List<File> filesToDelete = this.validateFiles(fileUUIDs, "MONEYTOR");
      FileDeleteResponse deleteResponse = new FileDeleteResponse();
      deleteResponse.setTotalRequested((long)fileUUIDs.size());
      int deleted = 0;
      Iterator var5 = filesToDelete.iterator();

      while(var5.hasNext()) {
         File file = (File)var5.next();

         try {
            FileManager fileManager = this.fileManagerFactory.getFileManager(file.getStorageSystem());
            fileManager.deleteFile(file);
            ++deleted;
         } catch (ApiException var8) {
            LOGGER.warn("Error deleting this file");
         }
      }

      deleteResponse.setTotalDeleted((long)deleted);
      this.fileRepository.delete(filesToDelete);
      return deleteResponse;
   }

   private File buildFileModel(FileUploadActionResponse response, String clientId) {
      File file = new File();
      file.setFileLocation(response.getFileLocation());
      file.setFileName(response.getFileName());
      file.setFileSizeBytes(response.getFileSizeBytes());
      file.setStorageSystem(response.getStorageSystem());
      file.setUuid(response.getFileUUID());
      file.setUploadedBy(clientId);
      file.setTimeAdded(LocalDateTime.now());
      file.setOriginalFileName(response.getOriginalFileName());
      return file;
   }

   private File validateFile(String fileUUID, String clientId) throws ApiException {
      if (!StringUtils.isBlank(fileUUID) && !StringUtils.isBlank(clientId)) {
         File file = this.fileRepository.findFirstByUuidAndUploadedBy(fileUUID, clientId);
         if (file == null) {
            throw new NotFoundApiException("File not found");
         } else if (file.getStorageSystem() == null) {
            throw new ApiException("No storage system found for file");
         } else {
            return file;
         }
      } else {
         throw new BadRequestApiException("FileUUID and ClientId must be provided");
      }
   }

   private List<File> validateFiles(List<String> fileUUIDs, String clientId) {
      List<String> uuids = (List)fileUUIDs.stream().filter((s) -> {
         return !StringUtils.isBlank(s);
      }).collect(Collectors.toList());
      return this.fileRepository.findAllByUuidInAndUploadedBy(uuids, clientId);
   }

   private FileManager getFileManager(String clientId) throws ApiException {
      Client client = this.clientRepository.findFirstByClientId(clientId);
      return client != null && client.getStorageSystem() != null ? this.fileManagerFactory.getFileManager(client.getStorageSystem()) : this.fileManagerFactory.getFileManager(StorageSystem.LOCALSTORAGE);
   }

   public FileDTO loadFileAsObjectWithInputStream(String fileUUID) throws ApiException, IOException {
      FileTypeDetector detector = new TikaFileTypeDetector();
      File file = this.validateFile(fileUUID, "MONEYTOR");
      FileManager fileManager = this.fileManagerFactory.getFileManager(file.getStorageSystem());
      Resource resource = fileManager.loadFile(file);
      FileDTO fileDTO = new FileDTO();

      String contentType;
      try {
         contentType = detector.probeContentType(Paths.get(file.getFileLocation() + "/" + file.getFileName()));
      } catch (IOException var9) {
         LOGGER.error("Error occurred while loading files as DTO due to ", var9);
         contentType = "UNKNOWN";
      }

      byte[] fileByteArray = new byte[resource.getInputStream().available()];
      resource.getInputStream().read(fileByteArray);
      fileDTO.setContentType(contentType);
      fileDTO.setFileLocation(file.getFileLocation());
      fileDTO.setFileName(file.getFileName());
      fileDTO.setOriginalFileName(file.getOriginalFileName());
      fileDTO.setFileSizeBytes(file.getFileSizeBytes());
      fileDTO.setFileUUID(file.getUuid());
      fileDTO.setUploadedTime(file.getTimeAdded().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
      fileDTO.setStorageSystem(file.getStorageSystem());
      fileDTO.setSuccessful(true);
      fileDTO.setFileByteArray(fileByteArray);
      return fileDTO;
   }
}
