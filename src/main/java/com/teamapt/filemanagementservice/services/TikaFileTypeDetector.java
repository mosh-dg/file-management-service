package com.teamapt.filemanagementservice.services;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.spi.FileTypeDetector;
import org.apache.tika.Tika;

public class TikaFileTypeDetector extends FileTypeDetector {
   private final Tika tika = new Tika();

   public String probeContentType(Path path) throws IOException {
      String fileNameDetect = this.tika.detect(path.toString());
      if (!fileNameDetect.equals("application/octet-stream")) {
         return fileNameDetect;
      } else {
         String fileContentDetect = this.tika.detect(path.toFile());
         return !fileContentDetect.equals("application/octet-stream") ? fileContentDetect : null;
      }
   }
}
