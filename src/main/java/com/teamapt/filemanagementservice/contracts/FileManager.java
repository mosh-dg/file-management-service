package com.teamapt.filemanagementservice.contracts;

import com.teamapt.exceptions.ApiException;
import com.teamapt.filemanagementservice.model.entity.File;
import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.filemanagementservicelib.response.FileUploadActionResponse;
import java.util.List;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileManager {
   FileUploadActionResponse saveFile(MultipartFile var1) throws ApiException;

   FileUploadActionResponse saveFile(MultipartFile var1, String var2) throws ApiException;

   List<FileUploadActionResponse> saveFiles(List<MultipartFile> var1) throws ApiException;

   List<FileUploadActionResponse> saveFiles(List<MultipartFile> var1, String var2) throws ApiException;

   Resource loadFile(File var1) throws ApiException;

   boolean deleteFile(File var1) throws ApiException;

   StorageSystem getStorageSystem();

   void init() throws ApiException;
}
