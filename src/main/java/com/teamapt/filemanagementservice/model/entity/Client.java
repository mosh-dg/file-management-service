package com.teamapt.filemanagementservice.model.entity;

import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.jpa.helpers.converters.LocalDateTimeConverter;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(
   name = "client"
)
public class Client {
   @Id
   @GeneratedValue(
      strategy = GenerationType.IDENTITY
   )
   private Long id;
   @Column(
      nullable = false,
      unique = true
   )
   private String clientId;
   @Column(
      nullable = false
   )
   private String filesLocation;
   @NotNull
   @Enumerated(EnumType.STRING)
   private StorageSystem storageSystem;
   @Convert(
      converter = LocalDateTimeConverter.class
   )
   private LocalDateTime timeAdded;

   public Long getId() {
      return this.id;
   }

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getFilesLocation() {
      return this.filesLocation;
   }

   public void setFilesLocation(String filesLocation) {
      this.filesLocation = filesLocation;
   }

   public StorageSystem getStorageSystem() {
      return this.storageSystem;
   }

   public void setStorageSystem(StorageSystem storageSystem) {
      this.storageSystem = storageSystem;
   }

   public LocalDateTime getTimeAdded() {
      return this.timeAdded;
   }

   public void setTimeAdded(LocalDateTime timeAdded) {
      this.timeAdded = timeAdded;
   }
}
