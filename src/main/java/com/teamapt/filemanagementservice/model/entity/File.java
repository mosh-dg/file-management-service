package com.teamapt.filemanagementservice.model.entity;

import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.jpa.helpers.converters.LocalDateTimeConverter;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(
   name = "moneytor_file"
)
public class File {
   @Id
   @GeneratedValue(
      strategy = GenerationType.IDENTITY
   )
   private Long id;
   @Column(
      nullable = false
   )
   private String fileName;
   @Column(
      nullable = false
   )
   private String uuid;
   @Column(
      nullable = false
   )
   private String uploadedBy;
   @Column(
      nullable = false
   )
   private Long fileSizeBytes;
   @Column(
      nullable = false
   )
   @Enumerated(EnumType.STRING)
   private StorageSystem storageSystem;
   @Column(
      nullable = false,
      length = 512
   )
   private String fileLocation;
   @Column(
      nullable = false,
      length = 512
   )
   private String originalFileName;
   @Convert(
      converter = LocalDateTimeConverter.class
   )
   private LocalDateTime timeAdded;

   public Long getId() {
      return this.id;
   }

   public String getFileName() {
      return this.fileName;
   }

   public void setFileName(String fileName) {
      this.fileName = fileName;
   }

   public String getUuid() {
      return this.uuid;
   }

   public void setUuid(String uuid) {
      this.uuid = uuid;
   }

   public String getUploadedBy() {
      return this.uploadedBy;
   }

   public void setUploadedBy(String uploadedBy) {
      this.uploadedBy = uploadedBy;
   }

   public Long getFileSizeBytes() {
      return this.fileSizeBytes;
   }

   public void setFileSizeBytes(Long fileSizeBytes) {
      this.fileSizeBytes = fileSizeBytes;
   }

   public StorageSystem getStorageSystem() {
      return this.storageSystem;
   }

   public void setStorageSystem(StorageSystem storageSystem) {
      this.storageSystem = storageSystem;
   }

   public String getFileLocation() {
      return this.fileLocation;
   }

   public void setFileLocation(String fileLocation) {
      this.fileLocation = fileLocation;
   }

   public LocalDateTime getTimeAdded() {
      return this.timeAdded;
   }

   public void setTimeAdded(LocalDateTime timeAdded) {
      this.timeAdded = timeAdded;
   }

   public String getOriginalFileName() {
      return this.originalFileName;
   }

   public void setOriginalFileName(String originalFileName) {
      this.originalFileName = originalFileName;
   }
}
