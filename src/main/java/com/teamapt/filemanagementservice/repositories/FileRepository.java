package com.teamapt.filemanagementservice.repositories;

import com.teamapt.filemanagementservice.model.entity.File;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
   File findFirstByUuidAndUploadedBy(String var1, String var2);

   List<File> findAllByUuidInAndUploadedBy(List<String> var1, String var2);
}
