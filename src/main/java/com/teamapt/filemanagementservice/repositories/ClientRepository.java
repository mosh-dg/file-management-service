package com.teamapt.filemanagementservice.repositories;

import com.teamapt.filemanagementservice.model.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
   Client findFirstByClientId(String var1);
}
