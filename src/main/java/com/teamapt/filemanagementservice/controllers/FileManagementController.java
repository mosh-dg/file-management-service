package com.teamapt.filemanagementservice.controllers;

import com.teamapt.exceptions.ApiException;
import com.teamapt.filemanagementservice.services.FileManagementService;
import com.teamapt.filemanagementservice.services.TikaFileTypeDetector;
import com.teamapt.filemanagementservicelib.request.FileDeleteRequest;
import com.teamapt.filemanagementservicelib.request.FileUploadRequest;
import com.teamapt.filemanagementservicelib.response.FileDTO;
import com.teamapt.filemanagementservicelib.response.FileDTOWrapper;
import com.teamapt.filemanagementservicelib.response.FileDeleteResponse;
import com.teamapt.filemanagementservicelib.response.FileUploadResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.spi.FileTypeDetector;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/api/v1/file-manager"})
public class FileManagementController {
   private final FileManagementService fileManagementService;

   @Autowired
   public FileManagementController(FileManagementService fileManagementService) {
      this.fileManagementService = fileManagementService;
   }

   @RequestMapping(
      value = {"/save"},
      method = {RequestMethod.POST}
   )
   public FileUploadResponse uploadFile(@ModelAttribute @Validated FileUploadRequest request) throws ApiException {
      return this.fileManagementService.saveFile(request);
   }

   @RequestMapping(
      value = {"/save-multiple"},
      method = {RequestMethod.POST}
   )
   public FileUploadResponse uploadFiles(@ModelAttribute @Validated FileUploadRequest request) throws ApiException {
      return this.fileManagementService.saveFiles(request);
   }

   @ResponseBody
   @RequestMapping(
      value = {"/get-dto"},
      method = {RequestMethod.GET}
   )
   public FileDTO loadFileAsObject(@RequestParam(name = "file") String fileUUID) throws ApiException {
      return this.fileManagementService.loadFileAsDTO(fileUUID);
   }

   @ResponseBody
   @RequestMapping(
      value = {"/get-dto/multiple"},
      method = {RequestMethod.GET}
   )
   public FileDTOWrapper loadFileAsObject(@RequestParam(name = "file") List<String> fileUUIDs) throws ApiException {
      return this.fileManagementService.loadFilesAsDTO(fileUUIDs);
   }

   @ResponseBody
   @RequestMapping(
      value = {"/get"},
      method = {RequestMethod.GET}
   )
   public void loadFile(@RequestParam(name = "file") String fileUUID, HttpServletResponse response) throws ApiException {
      Resource file = this.fileManagementService.loadFile(fileUUID);

      try {
         InputStream inputStream = file.getInputStream();
         FileTypeDetector detector = new TikaFileTypeDetector();
         String contentType = detector.probeContentType(file.getFile().toPath());
         response.setContentType(contentType);
         response.setHeader("Content-Disposition", "attachment;");
         IOUtils.copy(inputStream, response.getOutputStream());
         response.flushBuffer();
         inputStream.close();
      } catch (IOException var7) {
         throw new ApiException("Error loading file", var7);
      }
   }

   @RequestMapping(
      value = {"/delete"},
      method = {RequestMethod.POST}
   )
   public FileDeleteResponse deleteFiles(@RequestBody FileDeleteRequest request) {
      return this.fileManagementService.deleteFiles(request.getFileUUIDs());
   }

   @ResponseBody
   @RequestMapping(
      value = {"/get-dto-with-inputstream"},
      method = {RequestMethod.GET}
   )
   public FileDTO loadFileAsObjectWithInputStream(@RequestParam(name = "file") String fileUUID) throws ApiException {
      try {
         return this.fileManagementService.loadFileAsObjectWithInputStream(fileUUID);
      } catch (IOException var3) {
         throw new ApiException("Error loading file", var3);
      }
   }
}
