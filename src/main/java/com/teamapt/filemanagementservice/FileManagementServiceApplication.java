package com.teamapt.filemanagementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(
   scanBasePackages = {"com.teamapt"}
)
@EnableAsync
@EnableJpaRepositories(
   basePackages = {"com.teamapt.filemanagementservice", "com.teamapt.filemanagementservicelib"}
)
public class FileManagementServiceApplication {
   public static void main(String[] args) {
      SpringApplication.run(FileManagementServiceApplication.class, args);
   }
}
