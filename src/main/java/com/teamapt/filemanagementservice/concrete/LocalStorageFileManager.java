package com.teamapt.filemanagementservice.concrete;

import com.teamapt.exceptions.ApiException;
import com.teamapt.exceptions.BadRequestApiException;
import com.teamapt.filemanagementservice.contracts.FileManager;
import com.teamapt.filemanagementservice.model.entity.File;
import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.filemanagementservicelib.response.FileUploadActionResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class LocalStorageFileManager implements FileManager {
   private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
   private Path rootLocation;
   private Path trashLocation;
   @Value("${storage.path:filestorage}")
   private String defaultSavePath;
   @Value("${default.storage.path:public}")
   private String publicFolder;
   @Value("${default.trash.path:trash}")
   private String trashPath;

   @PostConstruct
   public void construct() {
      this.rootLocation = Paths.get(this.defaultSavePath);
      this.trashLocation = Paths.get(this.trashPath);
   }

   public FileUploadActionResponse saveFile(MultipartFile file) throws ApiException {
      return this.saveFile(file, this.publicFolder);
   }

   public FileUploadActionResponse saveFile(MultipartFile file, String saveLocation) throws ApiException {
      if (file.isEmpty()) {
         throw new BadRequestApiException("File is Empty");
      } else if (StringUtils.isBlank(file.getOriginalFilename())) {
         throw new BadRequestApiException("File name is Empty");
      } else {
         String fileLocation;
         if (StringUtils.isBlank(saveLocation)) {
            fileLocation = String.format("/%s/", this.publicFolder);
         } else {
            fileLocation = saveLocation.startsWith("/") ? saveLocation : String.format("/%s", saveLocation);
            fileLocation = fileLocation.endsWith("/") ? fileLocation : String.format("%s/", fileLocation);
         }

         fileLocation = fileLocation.substring(1, fileLocation.length() - 1);
         String fullLocationPath = String.format("%s/%s", this.defaultSavePath, fileLocation);
         Path location = Paths.get(fullLocationPath);

         try {
            Files.createDirectories(location);
         } catch (IOException var12) {
            this.logger.warn("Unable to create folder due to ", var12);
            throw new ApiException("Error creating folders");
         }

         String originalName = FilenameUtils.getBaseName(file.getOriginalFilename());
         if (org.springframework.util.StringUtils.isEmpty(originalName)) {
            throw new ApiException("File name cannot be blank");
         } else {
            String ext = this.getFileExtension(file);
            if (org.springframework.util.StringUtils.isEmpty(ext)) {
               throw new ApiException("Extension not found for file");
            } else {
               String randStr = UUID.randomUUID().toString();
               String newFileName = String.format("%s_%s.%s", originalName, randStr, ext);

               try {
                  Files.copy(file.getInputStream(), location.resolve(newFileName), new CopyOption[0]);
                  FileUploadActionResponse response = new FileUploadActionResponse();
                  response.setFileLocation(fileLocation);
                  response.setFileName(newFileName);
                  response.setFileSizeBytes(file.getSize());
                  response.setFileUUID(randStr);
                  response.setOriginalFileName(originalName);
                  response.setStorageSystem(this.getStorageSystem());
                  response.setSuccessful(true);
                  return response;
               } catch (IOException var11) {
                  this.logger.error("Error occurred while uploading file due to ", var11);
                  throw new ApiException("Error uploading file");
               }
            }
         }
      }
   }

   public List<FileUploadActionResponse> saveFiles(List<MultipartFile> files) throws ApiException {
      return this.saveFiles(files, this.publicFolder);
   }

   public List<FileUploadActionResponse> saveFiles(List<MultipartFile> files, String fileLocation) throws ApiException {
      List<FileUploadActionResponse> responses = new ArrayList();
      if (files != null && !files.isEmpty()) {
         FileUploadActionResponse response;
         for(Iterator var4 = files.iterator(); var4.hasNext(); responses.add(response)) {
            MultipartFile file = (MultipartFile)var4.next();

            try {
               response = this.saveFile(file, fileLocation);
            } catch (ApiException var8) {
               this.logger.error("Error occurred while saving files due to ", var8);
               response = new FileUploadActionResponse();
               response.setFileSizeBytes(file.getSize());
               response.setOriginalFileName(FilenameUtils.getBaseName(file.getOriginalFilename()));
               response.setStorageSystem(this.getStorageSystem());
               response.setSuccessful(false);
            }
         }

         return responses;
      } else {
         return responses;
      }
   }

   public Resource loadFile(File file) throws ApiException {
      if (file != null && file.getFileLocation() != null && file.getStorageSystem() == this.getStorageSystem()) {
         Path location = Paths.get(this.defaultSavePath);
         String fileLocation = file.getFileLocation();
         if (!StringUtils.isBlank(fileLocation)) {
            location = Paths.get(this.defaultSavePath + "/" + fileLocation);
         }

         try {
            Path filePath = location.resolve(file.getFileName());
            Resource resource = new UrlResource(filePath.toUri());
            if (!resource.exists() && !resource.isReadable()) {
               throw new ApiException("Could'nt load file");
            } else {
               return resource;
            }
         } catch (MalformedURLException var6) {
            this.logger.error("Error occurred due to ", var6);
            throw new ApiException("Error! -> message = " + var6.getMessage());
         }
      } else {
         throw new BadRequestApiException("File has incomplete parameters");
      }
   }

   public boolean deleteFile(File file) throws ApiException {
      if (file != null && file.getFileLocation() != null && file.getStorageSystem() == this.getStorageSystem()) {
         String fileLocation = file.getFileLocation();
         String fileName = file.getFileName();

         try {
            FileSystemUtils.copyRecursively(this.rootLocation.resolve(fileLocation + "/" + fileName).toFile(), this.trashLocation.resolve(fileName).toFile());
         } catch (Exception var5) {
            this.logger.warn("Could not copy to trash due to ", var5);
         }

         FileSystemUtils.deleteRecursively(this.rootLocation.resolve(fileLocation + "/" + fileName).toFile());
         return true;
      } else {
         throw new BadRequestApiException("File has incomplete parameters");
      }
   }

   public void init() throws ApiException {
      try {
         Files.createDirectories(this.rootLocation);
         Files.createDirectories(this.trashLocation);
      } catch (IOException var2) {
         this.logger.warn("Error occurred due to ", var2);
         throw new ApiException("Could not initialize storage", var2);
      }
   }

   private String getFileExtension(MultipartFile file) {
      String fileName = file.getOriginalFilename();
      return fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0 ? fileName.substring(fileName.lastIndexOf(".") + 1) : "";
   }

   public StorageSystem getStorageSystem() {
      return StorageSystem.LOCALSTORAGE;
   }
}
