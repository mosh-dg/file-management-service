package com.teamapt.filemanagementservice.concrete;

import com.teamapt.exceptions.ApiException;
import com.teamapt.filemanagementservice.contracts.FileManager;
import com.teamapt.filemanagementservice.model.entity.File;
import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import com.teamapt.filemanagementservicelib.response.FileUploadActionResponse;
import java.util.List;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FtpFileManager implements FileManager {
   public FileUploadActionResponse saveFile(MultipartFile file) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public FileUploadActionResponse saveFile(MultipartFile file, String fileLocation) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public List<FileUploadActionResponse> saveFiles(List<MultipartFile> files) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public List<FileUploadActionResponse> saveFiles(List<MultipartFile> files, String fileLocation) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public Resource loadFile(File file) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public boolean deleteFile(File file) throws ApiException {
      throw new UnsupportedOperationException();
   }

   public StorageSystem getStorageSystem() {
      return StorageSystem.FTP;
   }

   public void init() throws ApiException {
      throw new UnsupportedOperationException();
   }
}
