package com.teamapt.filemanagementservice.concrete;

import com.teamapt.exceptions.ApiException;
import com.teamapt.filemanagementservice.contracts.FileManager;
import com.teamapt.filemanagementservicelib.enums.StorageSystem;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileManagerFactory {
   private final List<FileManager> fileManagers;

   @Autowired
   public FileManagerFactory(List<FileManager> fileManagers) {
      this.fileManagers = fileManagers;
   }

   public FileManager getFileManager(StorageSystem storageSystem) throws ApiException {
      Optional<FileManager> fileManager = this.fileManagers.stream().filter((f) -> {
         return f.getStorageSystem().equals(storageSystem);
      }).findFirst();
      if (fileManager.isPresent()) {
         return (FileManager)fileManager.get();
      } else {
         throw new ApiException(storageSystem + " not supported");
      }
   }
}
